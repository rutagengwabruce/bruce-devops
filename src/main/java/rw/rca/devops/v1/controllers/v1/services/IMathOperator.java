package rw.rca.devops.v1.controllers.v1.services;

import rw.rca.devops.v1.controllers.v1.exceptions.InvalidOperationException;

public interface IMathOperator {

    double doMath(double operand1, double operand2, String operation) throws InvalidOperationException;
}
